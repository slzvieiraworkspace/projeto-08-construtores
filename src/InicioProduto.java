
public class InicioProduto {

    public static void main(String[] args) {
        
        Produto p1 = new Produto();
        Produto p2 = new Produto(3015, "Picanha", "Carnes", 34.5);
        
        p1.getPreco();
        p2.getCategoria();
    }
}
