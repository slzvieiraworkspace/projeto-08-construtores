
public class Inicioarray {

    public static void main(String[] args) {

        Funcionario[] f = {
                           new Funcionario("Jos�", "Carlos", "Programador", 5000.00),
                           new Funcionario("Jo�o", "Pedro", "Programador", 5000.00),
                           new Funcionario("Pedro", "Silva", "Programador", 4500.00),
                           new Funcionario("Carlos", "Eduardo", "Troxa", 2000.00)
        };

        for (Funcionario i : f) {
            System.out.println(i.getDados());
        }
    }
}
