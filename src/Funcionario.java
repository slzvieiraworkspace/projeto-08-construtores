
public class Funcionario {

		private String nome;
		private	String sobrenome  ;
		private String cargo  ;
		private double salario ;
		private String dados;
		
		
		public Funcionario() {}
		
		public Funcionario(String nu, String sb, String car, double sal) {
			nome = nu;
			sobrenome = sb;
			cargo = car;
			salario = sal;	
			dados = nu+ " " + " " + sb +" "+ car + " - " + sal;
		}
			
	public String getNome() {
		return nome;
	}

	public void setNome(String n) {
		nome = n;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String s) {
		sobrenome = s;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String c) {
		cargo = c;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double sal) {
		salario = sal;
	}
	
	public String getDados() {
		return dados; 
	}
		
}

